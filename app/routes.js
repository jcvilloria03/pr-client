/* eslint-disable import/no-unresolved */
// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';
import { auth } from 'utils/AuthService';
import { BASE_PATH_NAME, BASE_PATH_NAME_MF } from './constants';

const errorLoading = ( err ) => {
    console.error( 'Dynamic page loading failed', err ); // eslint-disable-line no-console
};

const loadModule = ( cb ) => ( componentModule ) => {
    cb( null, componentModule.default );
};

/**
 * Creates routes.
 */
export default function createRoutes( store ) {
  // Create reusable async injectors using getAsyncInjectors factory
    const { injectReducer, injectSagas } = getAsyncInjectors( store ); // eslint-disable-line no-unused-vars

    return [
        {
            path: '/',
            indexRoute: { onEnter: ( nextState, replace ) => replace( BASE_PATH_NAME ) }
        }, {
            path: `${BASE_PATH_NAME}`,
            name: 'Home',
            indexRoute: { onEnter: ( nextState, replace ) => replace( '/login' ) }
        }, {
            path: '/login',
            name: 'Login',
            // anyDevice: true,
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Login' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/signup',
            name: 'Sign Up',
            // anyDevice: true,
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Signup/reducer' ),
                    import( 'containers/Signup/sagas' ),
                    import( 'containers/Signup' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, componentReducer, componentSaga, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'signUp', componentReducer.default );
                    injectSagas( componentSaga.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/welcome',
            name: 'Welcome',
            // anyDevice: true,
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Welcome' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/verify',
            name: 'Verify',
            // anyDevice: true,
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Verify' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/dashboard',
            name: 'dashboard',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Dashboard/reducer' ),
                    import( 'containers/Dashboard/sagas' ),
                    import( 'containers/Dashboard' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, dashboardReducer, dashboardSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'dashboard', dashboardReducer.default );
                    injectSagas( dashboardSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/profile',
            name: 'userProfile',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/UserProfile/reducer' ),
                    import( 'containers/UserProfile/sagas' ),
                    import( 'containers/UserProfile' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'userProfile', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/payroll`,
            name: 'Payroll',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Payroll/List/reducer' ),
                    import( 'containers/Payroll/List/sagas' ),
                    import( 'containers/Payroll/List' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, payrollHistoryReducer, payrollHistorySagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'payrollHistory', payrollHistoryReducer.default );
                    injectSagas( payrollHistorySagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/payroll/generate`,
            name: 'Generate Payroll',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Payroll/Generate/reducer' ),
                    import( 'containers/Payroll/Generate/sagas' ),
                    import( 'containers/Payroll/Generate' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, payrollReducer, payrollSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'generate', payrollReducer.default );
                    injectSagas( payrollSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/payroll/:id`,
            name: 'View Payroll',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Payroll/View/reducer' ),
                    import( 'containers/Payroll/View/sagas' ),
                    import( 'containers/Payroll/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, payrollReducer, payrollSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'view', payrollReducer.default );
                    injectSagas( payrollSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/payroll/:id/transaction`,
            name: 'Gap Loan Transaction',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GapLoanTransaction/View/reducer' ),
                    import( 'containers/GapLoanTransaction/View/sagas' ),
                    import( 'containers/GapLoanTransaction/View' )
                ]);

                const renderRoute = loadModule( cb );
                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, payrollReducer, payrollSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'gapLoanCandidates', payrollReducer.default );
                    injectSagas( payrollSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/payroll/:id/transaction/edit-amortization`,
            name: 'Edit Gap Loan Amortization',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GapLoanTransaction/View/reducer' ),
                    import( 'containers/GapLoanTransaction/View/sagas' ),
                    import( 'containers/GapLoanTransaction/View/Amortization' )
                ]);

                const renderRoute = loadModule( cb );
                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, payrollReducer, payrollSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'gapLoanCandidates', payrollReducer.default );
                    injectSagas( payrollSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/commissions`,
            name: 'Commissions',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Commissions/View/reducer' ),
                    import( 'containers/Commissions/View/sagas' ),
                    import( 'containers/Commissions/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'commissions', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/commissions/add`,
            name: 'Add Commissions',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Commissions/Add/reducer' ),
                    import( 'containers/Commissions/Add/sagas' ),
                    import( 'containers/Commissions/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addCommissions', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/commissions/:id/edit`,
            name: 'Edit Commission',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Commissions/Edit/reducer' ),
                    import( 'containers/Commissions/Edit/sagas' ),
                    import( 'containers/Commissions/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editCommission', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/allowances`,
            name: 'Allowances',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Allowances/View/reducer' ),
                    import( 'containers/Allowances/View/sagas' ),
                    import( 'containers/Allowances/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'allowances', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/allowances/add`,
            name: 'Add Allowances',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Allowances/Add/reducer' ),
                    import( 'containers/Allowances/Add/sagas' ),
                    import( 'containers/Allowances/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addAllowances', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/allowances/:id/edit`,
            name: 'Edit Allowance',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Allowances/Edit/reducer' ),
                    import( 'containers/Allowances/Edit/sagas' ),
                    import( 'containers/Allowances/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editAllowance', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/payslip`,
            name: 'Payslip View',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Payroll/Payslip/reducer' ),
                    import( 'containers/Payroll/Payslip/sagas' ),
                    import( 'containers/Payroll/Payslip' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, payslipReducer, payslipSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'payslip', payslipReducer.default );
                    injectSagas( payslipSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/download(/:module/:id)`,
            name: 'Download View',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Download/reducer' ),
                    import( 'containers/Download/sagas' ),
                    import( 'containers/Download' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, downloadReducer, downloadSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'download', downloadReducer.default );
                    injectSagas( downloadSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms/SSS/LCL/:id`,
            name: 'View/Edit SSS Loan Forms',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/SSS/LCL/reducer' ),
                    import( 'containers/GovernmentForms/SSS/LCL/sagas' ),
                    import( 'containers/GovernmentForms/SSS/LCL/index' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, governmentFormsReducer, governmentFormsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'SSSLoanForms', governmentFormsReducer.default );
                    injectSagas( governmentFormsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms/SSS/Remittance/:id`,
            name: 'View/Edit SSS Loan Forms',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/SSS/Remittance/reducer' ),
                    import( 'containers/GovernmentForms/SSS/Remittance/sagas' ),
                    import( 'containers/GovernmentForms/SSS/Remittance/index' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, governmentFormsReducer, governmentFormsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'SSSRemittanceForms', governmentFormsReducer.default );
                    injectSagas( governmentFormsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms/HDMF/MSRF/:id`,
            name: 'View/Edit MSRF Form',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/HDMF/MSRF/reducer' ),
                    import( 'containers/GovernmentForms/HDMF/MSRF/sagas' ),
                    import( 'containers/GovernmentForms/HDMF/MSRF/index' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, governmentFormsReducer, governmentFormsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'HDMFMSRFForms', governmentFormsReducer.default );
                    injectSagas( governmentFormsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms/HDMF/STLRF/:id`,
            name: 'View/Edit STRLF Form',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/HDMF/STLRF/reducer' ),
                    import( 'containers/GovernmentForms/HDMF/STLRF/sagas' ),
                    import( 'containers/GovernmentForms/HDMF/STLRF/index' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, governmentFormsReducer, governmentFormsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'HDMFSTLRFForms', governmentFormsReducer.default );
                    injectSagas( governmentFormsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms/PhilHealth/MCL/:id`,
            name: 'Generate PhilHealth Forms',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/PhilHealth/reducer' ),
                    import( 'containers/GovernmentForms/PhilHealth/sagas' ),
                    import( 'containers/GovernmentForms/PhilHealth' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, governmentFormsReducer, governmentFormsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'PhilHealthForms', governmentFormsReducer.default );
                    injectSagas( governmentFormsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms/BIR/1601C/:id`,
            name: 'Generate BIR1601C Forms',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/BIR/1601C/reducer' ),
                    import( 'containers/GovernmentForms/BIR/1601C/sagas' ),
                    import( 'containers/GovernmentForms/BIR/1601C' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, governmentFormsReducer, governmentFormsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'BIR1601CForms', governmentFormsReducer.default );
                    injectSagas( governmentFormsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms/BIR/1601E`,
            name: 'Generate BIR1601E Forms',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/BIR/1601E/reducer' ),
                    import( 'containers/GovernmentForms/BIR/1601E/sagas' ),
                    import( 'containers/GovernmentForms/BIR/1601E' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, governmentFormsReducer, governmentFormsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'BIR1601EForms', governmentFormsReducer.default );
                    injectSagas( governmentFormsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms/BIR/1601F`,
            name: 'Generate BIR1601F Forms',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/BIR/1601F/reducer' ),
                    import( 'containers/GovernmentForms/BIR/1601F/sagas' ),
                    import( 'containers/GovernmentForms/BIR/1601F' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, governmentFormsReducer, governmentFormsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'BIR1601FForms', governmentFormsReducer.default );
                    injectSagas( governmentFormsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms/BIR/1604C/:id`,
            name: 'Generate BIR1604C Form',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/BIR/1604C/reducer' ),
                    import( 'containers/GovernmentForms/BIR/1604C/sagas' ),
                    import( 'containers/GovernmentForms/BIR/1604C' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'governmentFormBIR1604C', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms`,
            name: 'Forms',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/View/reducer' ),
                    import( 'containers/GovernmentForms/View/sagas' ),
                    import( 'containers/GovernmentForms/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, governmentFormsReducer, governmentFormsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'government_forms', governmentFormsReducer.default );
                    injectSagas( governmentFormsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/forms/generate`,
            name: 'Forms',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/GovernmentForms/Generate/reducer' ),
                    import( 'containers/GovernmentForms/Generate/sagas' ),
                    import( 'containers/GovernmentForms/Generate' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, governmentFormsReducer, governmentFormsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'government_forms_generate', governmentFormsReducer.default );
                    injectSagas( governmentFormsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employees/add',
            name: 'Add Employee',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/AddEmployee/reducer' ),
                    import( 'containers/AddEmployee/sagas' ),
                    import( 'containers/AddEmployee' )
                ]);
                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, addEmployeeReducer, addEmployeeSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'add_employee', addEmployeeReducer.default );
                    injectSagas( addEmployeeSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        },

        // {
        //    old batch add of employees codes
        //
        //     path: '/employees/batch-add',
        //     name: 'Batch Add Employee',
        //     getComponent( nextState, cb ) {
        //         const importModules = Promise.all([
        //             import( 'containers/App/reducer' ),
        //             import( 'containers/App/sagas' ),
        //             import( 'containers/BatchUpload/reducer' ),
        //             import( 'containers/BatchUpload/sagas' ),
        //             import( 'containers/BatchUpload' )
        //         ]);
        //         const renderRoute = loadModule( cb );

        //         importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, batchUploadReducer, batchUploadSagas, component ]) => {
        //             injectReducer( 'topLevel', appReducer.default );
        //             injectSagas( appSagas.default );
        //             injectReducer( 'batch_add', batchUploadReducer.default );
        //             injectSagas( batchUploadSagas.default );
        //             renderRoute( component );
        //         });

        //         importModules.catch( errorLoading );
        //     }
        // },{
        //     path: '/employees/batch-update',
        //     name: 'Batch Update Employee',
        //     getComponent( nextState, cb ) {
        //         const importModules = Promise.all([
        //             import( 'containers/App/reducer' ),
        //             import( 'containers/App/sagas' ),
        //             import( 'containers/BatchUpdate/reducer' ),
        //             import( 'containers/BatchUpdate/sagas' ),
        //             import( 'containers/BatchUpdate' )
        //         ]);
        //         const renderRoute = loadModule( cb );

        //         importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, batchUpdateReducer, batchUpdateSagas, component ]) => {
        //             injectReducer( 'topLevel', appReducer.default );
        //             injectSagas( appSagas.default );
        //             injectReducer( 'batch_update', batchUpdateReducer.default );
        //             injectSagas( batchUpdateSagas.default );
        //             renderRoute( component );
        //         });

        //         importModules.catch( errorLoading );
        //     }
        // },

        {
            path: '/employees/batch-add',
            name: 'Batch Add Employee',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Employees/BatchUpload/reducer.js' ),
                    import( 'containers/Employees/BatchUpload/sagas' ),
                    import( 'containers/Employees/BatchUpload' )
                ]);
                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, batchUploadReducer, batchUploadSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'batch_add', batchUploadReducer.default );
                    injectSagas( batchUploadSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employees/batch-update',
            name: 'Batch Update Employee',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Employees/BatchUpdate/reducer' ),
                    import( 'containers/Employees/BatchUpdate/sagas' ),
                    import( 'containers/Employees/BatchUpdate' )
                ]);
                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, batchUpdateReducer, batchUpdateSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'batch_update', batchUpdateReducer.default );
                    injectSagas( batchUpdateSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee/:id',
            name: 'Profile',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Employee/View/reducer' ),
                    import( 'containers/Employee/View/sagas' ),
                    import( 'containers/Employee/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, employeeReducer, employeeSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'employee_profile', employeeReducer.default );
                    injectSagas( employeeSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee/:id/terminate',
            name: 'Add Employee Termination',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/EmployeeTermination/Add/reducer' ),
                    import( 'containers/EmployeeTermination/Add/sagas' ),
                    import( 'containers/EmployeeTermination/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addEmployeeTermination', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee/:id/termination/:terminationId/edit',
            name: 'Edit Employee Termination',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/EmployeeTermination/Edit/reducer' ),
                    import( 'containers/EmployeeTermination/Edit/sagas' ),
                    import( 'containers/EmployeeTermination/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editEmployeeTermination', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee/:id/termination/:terminationId/checklist',
            name: 'Employee Termination Checklist',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/EmployeeTermination/Checklist/reducer' ),
                    import( 'containers/EmployeeTermination/Checklist/sagas' ),
                    import( 'containers/EmployeeTermination/Checklist' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'employeeTerminationChecklist', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee/:id/termination/:terminationId/computation',
            name: 'Employee Termination Computation',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/EmployeeTermination/Computation/reducer' ),
                    import( 'containers/EmployeeTermination/Computation/sagas' ),
                    import( 'containers/EmployeeTermination/Computation' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'employeeTerminationComputation', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/earnings`,
            name: 'View Earnings',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Earnings/View/reducer' ),
                    import( 'containers/Earnings/View/sagas' ),
                    import( 'containers/Earnings/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewEarningsReducer, viewEarningsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'viewEarnings', viewEarningsReducer.default );
                    injectSagas( viewEarningsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/earnings/batch-update`,
            name: 'Batch Update Earnings',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Earnings/BatchUpdate/reducer' ),
                    import( 'containers/Earnings/BatchUpdate/sagas' ),
                    import( 'containers/Earnings/BatchUpdate' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, batchUpdateEarningsReducer, batchUpdateEarningsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'batchUpdateEarnings', batchUpdateEarningsReducer.default );
                    injectSagas( batchUpdateEarningsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/earnings/:earningId/detail`,
            name: 'Earning Detail',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Earnings/Detail/reducer' ),
                    import( 'containers/Earnings/Detail/sagas' ),
                    import( 'containers/Earnings/Detail' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, earningsDetailReducer, earningsDetailSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'earningsDetail', earningsDetailReducer.default );
                    injectSagas( earningsDetailSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/earnings/:earningId/edit`,
            name: 'Edit Earning',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Earnings/Edit/reducer' ),
                    import( 'containers/Earnings/Edit/sagas' ),
                    import( 'containers/Earnings/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, editEarningsReducer, editEarningsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editEarnings', editEarningsReducer.default );
                    injectSagas( editEarningsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee/:id/basic-pay-adjustments/add',
            name: 'Add Basic Pay Adjustment',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/BasicPayAdjustments/Add/reducer' ),
                    import( 'containers/BasicPayAdjustments/Add/sagas' ),
                    import( 'containers/BasicPayAdjustments/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, addBasicPayAdjustmentReducer, addBasicPayAdjustmentSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addBasicPayAdjustment', addBasicPayAdjustmentReducer.default );
                    injectSagas( addBasicPayAdjustmentSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee/:id/basic-pay-adjustments/:adjustmentId/detail',
            name: 'Basic Pay Adjustment Detail',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/BasicPayAdjustments/Detail/reducer' ),
                    import( 'containers/BasicPayAdjustments/Detail/sagas' ),
                    import( 'containers/BasicPayAdjustments/Detail' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, basicPayAdjustmentDetailReducer, basicPayAdjustmentDetailSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'basicPayAdjustmentDetail', basicPayAdjustmentDetailReducer.default );
                    injectSagas( basicPayAdjustmentDetailSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee/:id/basic-pay-adjustments/:adjustmentId/edit',
            name: 'Edit Basic Pay Adjustment',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/BasicPayAdjustments/Edit/reducer' ),
                    import( 'containers/BasicPayAdjustments/Edit/sagas' ),
                    import( 'containers/BasicPayAdjustments/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, editBasicPayAdjustmentReducer, editBasicPayAdjustmentSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editBasicPayAdjustment', editBasicPayAdjustmentReducer.default );
                    injectSagas( editBasicPayAdjustmentSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employees',
            name: 'Employees',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Employees/View/reducer' ),
                    import( 'containers/Employees/View/sagas' ),
                    import( 'containers/Employees/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, employeesReducer, employeesSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'employees', employeesReducer.default );
                    injectSagas( employeesSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/employees`,
            indexRoute: { onEnter: ( nextState, replace ) => replace( '/employees' ) }
        }, {
            path: `${BASE_PATH_NAME}/users`,
            name: 'Users',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Users/reducer' ),
                    import( 'containers/Users/sagas' ),
                    import( 'containers/Users' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, usersReducer, usersSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'users', usersSagas.default );
                    injectSagas( usersReducer.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/roles`,
            name: 'Roles',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Roles/reducer' ),
                    import( 'containers/Roles/sagas' ),
                    import( 'containers/Roles' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, rolesReducer, rolesSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'roles', rolesReducer.default );
                    injectSagas( rolesSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/role/create`,
            name: 'Create Role',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Roles/Create/reducer' ),
                    import( 'containers/Roles/Create/sagas' ),
                    import( 'containers/Roles/Create' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, rolesReducer, rolesSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'createRole', rolesReducer.default );
                    injectSagas( rolesSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/role/:id/edit`,
            name: 'Edit Role',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Roles/Create/reducer' ),
                    import( 'containers/Roles/Create/sagas' ),
                    import( 'containers/Roles/Create' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, rolesReducer, rolesSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'createRole', rolesReducer.default );
                    injectSagas( rolesSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/views/create`,
            name: 'Create View',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Views/Create/reducer' ),
                    import( 'containers/Views/Create/sagas' ),
                    import( 'containers/Views/Create' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'createView', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/views/edit/:id`,
            name: 'Edit View',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Views/Create/reducer' ),
                    import( 'containers/Views/Create/sagas' ),
                    import( 'containers/Views/Create' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'createView', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/loans`,
            name: 'Loans',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Loans/View/reducer' ),
                    import( 'containers/Loans/View/sagas' ),
                    import( 'containers/Loans/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'loans', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/loans/add`,
            name: 'Add Loan',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Loans/Add/reducer' ),
                    import( 'containers/Loans/Add/sagas' ),
                    import( 'containers/Loans/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, addLoanReducer, addLoanSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addLoan', addLoanReducer.default );
                    injectSagas( addLoanSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/loans/add/edit-amortization`,
            name: 'Edit Loan Amortization',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Loans/Add/reducer' ),
                    import( 'containers/Loans/Add/sagas' ),
                    import( 'containers/Loans/Add/ManualEntry/Amortizations' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, addLoanReducer, addLoanSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addLoan', addLoanReducer.default );
                    injectSagas( addLoanSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/deductions`,
            name: 'Deductions',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Deductions/View/reducer' ),
                    import( 'containers/Deductions/View/sagas' ),
                    import( 'containers/Deductions/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'deductions', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/deductions/add`,
            name: 'Add Deduction',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Deductions/Add/reducer' ),
                    import( 'containers/Deductions/Add/sagas' ),
                    import( 'containers/Deductions/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, addDeductionReducer, addDeductionSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addDeduction', addDeductionReducer.default );
                    injectSagas( addDeductionSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/deductions/:id/edit`,
            name: 'Edit Deduction',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Deductions/Edit/reducer' ),
                    import( 'containers/Deductions/Edit/sagas' ),
                    import( 'containers/Deductions/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, editReducer, editSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editDeduction', editReducer.default );
                    injectSagas( editSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/loan-types`,
            name: 'Loan Types',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/LoanTypes/View/reducer' ),
                    import( 'containers/LoanTypes/View/sagas' ),
                    import( 'containers/LoanTypes/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'loanTypes', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/disbursements`,
            name: 'Disbursements',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Disbursements/View/reducer' ),
                    import( 'containers/Disbursements/View/sagas' ),
                    import( 'containers/Disbursements/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, disbursementsReducer, disbursementsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'disbursements', disbursementsReducer.default );
                    injectSagas( disbursementsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/disbursements/add`,
            name: 'Add Disbursements',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Disbursements/Add/reducer' ),
                    import( 'containers/Disbursements/Add/sagas' ),
                    import( 'containers/Disbursements/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, addDisbursementsReducer, addDisbursementsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addDisbursement', addDisbursementsReducer.default );
                    injectSagas( addDisbursementsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/adjustments`,
            name: 'Adjustments',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Adjustments/View/reducer' ),
                    import( 'containers/Adjustments/View/sagas' ),
                    import( 'containers/Adjustments/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'adjustments', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/adjustments/add`,
            name: 'Add Adjustment',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Adjustments/Add/reducer' ),
                    import( 'containers/Adjustments/Add/sagas' ),
                    import( 'containers/Adjustments/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, addAdjustmentReducer, addAdjustmentSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addAdjustment', addAdjustmentReducer.default );
                    injectSagas( addAdjustmentSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/adjustments/:id/edit`,
            name: 'Edit Adjustment',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Adjustments/Edit/reducer' ),
                    import( 'containers/Adjustments/Edit/sagas' ),
                    import( 'containers/Adjustments/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, editReducer, editSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editAdjustment', editReducer.default );
                    injectSagas( editSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/loan-types/add`,
            name: 'Add Loan Type',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/LoanTypes/Add' ),
                    import( 'containers/LoanTypes/Add/reducer' ),
                    import( 'containers/LoanTypes/Add/sagas' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component, manualReducer, manualSagas ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addLoanType', manualReducer.default );
                    injectSagas( manualSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/loans/:id/edit`,
            name: 'Edit Loan',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Loans/Edit/reducer' ),
                    import( 'containers/Loans/Edit/sagas' ),
                    import( 'containers/Loans/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, editReducer, editSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editLoan', editReducer.default );
                    injectSagas( editSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/loans/:id/edit-amortization`,
            name: 'Edit Loan Amortization',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Loans/Edit/reducer' ),
                    import( 'containers/Loans/Edit/sagas' ),
                    import( 'containers/Loans/Edit/Amortizations' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, editReducer, editSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editLoan', editReducer.default );
                    injectSagas( editSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/bonuses`,
            name: 'Bonuses',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Bonuses/View/reducer' ),
                    import( 'containers/Bonuses/View/sagas' ),
                    import( 'containers/Bonuses/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'bonuses', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/bonuses/add`,
            name: 'Add Bonus',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Bonuses/Add/reducer' ),
                    import( 'containers/Bonuses/Add/sagas' ),
                    import( 'containers/Bonuses/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, addBonusReducer, addBonusSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addBonus', addBonusReducer.default );
                    injectSagas( addBonusSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/bonuses/:id/edit`,
            name: 'Edit Bonus',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Bonuses/Edit/reducer' ),
                    import( 'containers/Bonuses/Edit/sagas' ),
                    import( 'containers/Bonuses/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editBonus', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/bonuses/:id/detail`,
            name: 'Bonus Details',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Bonuses/Detail/reducer' ),
                    import( 'containers/Bonuses/Detail/sagas' ),
                    import( 'containers/Bonuses/Detail' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'bonusesDetail', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/company/approvals/list',
            name: 'Approvals',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Approvals/reducer' ),
                    import( 'containers/Approvals/sagas' ),
                    import( 'containers/Approvals' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'approvals', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/attendance-computations',
            name: 'attendanceComputation',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/AttendanceComputation/View/reducer' ),
                    import( 'containers/AttendanceComputation/View/sagas' ),
                    import( 'containers/AttendanceComputation/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'attendanceComputation', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/attendance-computations/add/batch',
            name: 'attendanceComputationBatchUpload',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/AttendanceComputation/BatchUpload/reducer' ),
                    import( 'containers/AttendanceComputation/BatchUpload/sagas' ),
                    import( 'containers/AttendanceComputation/BatchUpload/' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'attendanceComputationBatchUpload', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/leaves',
            name: 'Leaves',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Leaves/View/reducer' ),
                    import( 'containers/Leaves/View/sagas' ),
                    import( 'containers/Leaves/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'leaves', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/leave-credits/add',
            name: 'Add Leave Credits',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Leaves/LeaveCredits/Add/reducer' ),
                    import( 'containers/Leaves/LeaveCredits/Add/sagas' ),
                    import( 'containers/Leaves/LeaveCredits/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addLeaveCredits', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/filed-leaves/add',
            name: 'Add Filed Leaves',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Leaves/FiledLeaves/Add/reducer' ),
                    import( 'containers/Leaves/FiledLeaves/Add/sagas' ),
                    import( 'containers/Leaves/FiledLeaves/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addFiledLeaves', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/filed-leaves/:id',
            name: 'Filed Leave Details',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Leaves/FiledLeaves/Details/reducer' ),
                    import( 'containers/Leaves/FiledLeaves/Details/sagas' ),
                    import( 'containers/Leaves/FiledLeaves/Details' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'filedLeaveDetails', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/filed-leaves/:id/edit',
            name: 'Edit Filed Leave',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Leaves/FiledLeaves/Edit/reducer' ),
                    import( 'containers/Leaves/FiledLeaves/Edit/sagas' ),
                    import( 'containers/Leaves/FiledLeaves/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'filedLeaveEdit', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/workflow-entitlements',
            name: 'Workflows',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/WorkflowEntitlements/View/reducer' ),
                    import( 'containers/WorkflowEntitlements/View/sagas' ),
                    import( 'containers/WorkflowEntitlements/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'workflowEntitlementsView', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/leaves/leave-credits/:id/edit',
            name: 'Edit Leave Credit',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Leaves/LeaveCredits/Edit/reducer' ),
                    import( 'containers/Leaves/LeaveCredits/Edit/sagas' ),
                    import( 'containers/Leaves/LeaveCredits/Edit/' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, editReducer, editSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editLeaveCredit', editReducer.default );
                    injectSagas( editSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/workflow-entitlements/add',
            name: 'Add Workflow Entitlement',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/WorkflowEntitlements/Add/reducer' ),
                    import( 'containers/WorkflowEntitlements/Add/sagas' ),
                    import( 'containers/WorkflowEntitlements/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([
                    appReducer,
                    idleTimerReducer, demoCompanyReducer, demoCompanySagas,
                    appSagas,
                    addWorkflowEntitlementReducer,
                    addWorkflowEntitlementSagas,
                    component
                ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addWorkflowEntitlement', addWorkflowEntitlementReducer.default );
                    injectSagas( addWorkflowEntitlementSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee-government-forms',
            name: 'Employee Government Forms',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/EmployeeGovernmentForms/List/reducer' ),
                    import( 'containers/EmployeeGovernmentForms/List/sagas' ),
                    import( 'containers/EmployeeGovernmentForms/List' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'employeeGovernmentFormsList', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee-government-forms/generate',
            name: 'Generate Employee Government Forms',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/EmployeeGovernmentForms/Generate/reducer' ),
                    import( 'containers/EmployeeGovernmentForms/Generate/sagas' ),
                    import( 'containers/EmployeeGovernmentForms/Generate' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'employeeGovernmentFormsGenerate', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/employee/:employeeId/government-forms/BIR/2316/:id',
            name: 'Generate BIR 2316 Form',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/EmployeeGovernmentForms/BIR/2316/reducer' ),
                    import( 'containers/EmployeeGovernmentForms/BIR/2316/sagas' ),
                    import( 'containers/EmployeeGovernmentForms/BIR/2316' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'employeeGovernmentFormsBIR2316', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/change-password',
            name: 'Change Password',
            // anyDevice: true,
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/ChangePassword/reducer' ),
                    import( 'containers/ChangePassword/sagas' ),
                    import( 'containers/ChangePassword' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, componentReducer, componentSaga, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'changePassword', componentReducer.default );
                    injectSagas( componentSaga.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/forgot-password',
            name: 'Forgot Password',
            indexRoute: { onEnter: ( nextState, replace ) => auth.loggedIn() && replace( '/' ) },
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/ForgotPassword/reducer' ),
                    import( 'containers/ForgotPassword/sagas' ),
                    import( 'containers/ForgotPassword' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, componentReducer, componentSaga, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'forgotPassword', componentReducer.default );
                    injectSagas( componentSaga.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/reset-password',
            name: 'Reset Password',
            indexRoute: { onEnter: ( nextState, replace ) => auth.storeAndRemoveResetPasswordQueryString( nextState, replace ) },
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/ResetPassword/reducer' ),
                    import( 'containers/ResetPassword/sagas' ),
                    import( 'containers/ResetPassword' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, componentReducer, componentSaga, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'resetPassword', componentReducer.default );
                    injectSagas( componentSaga.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides`,
            name: 'Guides',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/subscriptions`,
            name: 'Subscriptions Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Subscriptions' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/subscriptions/update`,
            name: 'Subscription Update Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Subscriptions/SubscriptionUpdate' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/employees`,
            name: 'Employee Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/EmployeeGuide' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/employees/batch-upload`,
            name: 'Employee Batch Upload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/EmployeeGuide/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/payroll`,
            name: 'Payroll Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Payroll' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/payroll/generate`,
            name: 'Payroll Generate Guide Index',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Payroll/Generate' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/payroll/generate/import`,
            name: 'Payroll Generate Import Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Payroll/Generate/Import' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/payroll/generate(/:guide)`,
            name: 'Payroll Generate Guide',
            getComponent( location, cb ) {
                let importModules = [
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' )
                ];

                switch ( location.params.guide.toLowerCase() ) {
                    case 'time-and-attendance':
                        importModules.push( import( 'containers/Guide/Payroll/Generate/Import/TimeAndAttendance' ) );
                        break;
                    case 'ta-overtime-holiday':
                        importModules.push( import( 'containers/Guide/Payroll/Generate/Import/TaOvertimeHoliday' ) );
                        break;
                    case 'bonuses':
                        importModules.push( import( 'containers/Guide/Payroll/Generate/Import/Bonus' ) );
                        break;
                    case 'allowances':
                        importModules.push( import( 'containers/Guide/Payroll/Generate/Import/Allowance' ) );
                        break;
                    case 'commissions':
                        importModules.push( import( 'containers/Guide/Payroll/Generate/Import/Commission' ) );
                        break;
                    case 'deductions':
                        importModules.push( import( 'containers/Guide/Payroll/Generate/Import/Deduction' ) );
                        break;
                    case 'adjustments':
                        importModules.push( import( 'containers/Guide/Payroll/Generate/Import/Adjustment' ) );
                        break;
                    default:
                        break;
                }

                importModules = Promise.all( importModules );

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/loans/`,
            name: 'Loans Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Loans' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/loans/batch-upload`,
            name: 'Loans Batch Upload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Loans/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/adjustments/`,
            name: 'Adjustments Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Adjustments' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/adjustments/batch-upload`,
            name: 'Adjustments Batc hUpload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Adjustments/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/deductions/`,
            name: 'Deductions Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Deductions' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/deductions/batch-upload`,
            name: 'Deductions Batch Upload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Deductions/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/bonuses/`,
            name: 'Bonuses Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Bonuses' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/bonuses/batch-upload`,
            name: 'Bonuses Batch Upload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Bonuses/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/allowances/`,
            name: 'Allowances Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Allowances' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/allowances/batch-upload`,
            name: 'Allowances Batch Upload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Allowances/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/earnings/`,
            name: 'Earnings Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Earnings' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/earnings/batch-update`,
            name: 'Earnings Batch Update Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Earnings/BatchUpdate' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/commissions/`,
            name: 'Commissions Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Commissions' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/commissions/batch-upload`,
            name: 'Commissions Batc hUpload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Commissions/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/leaves`,
            name: 'Leaves Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Leaves' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/leaves/leave-credits`,
            name: 'Leave Credits Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Leaves/LeaveCredits' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/leaves/filed-leaves`,
            name: 'Filed Leaves Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Leaves/FiledLeaves' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/leaves/leave-credits/batch-upload`,
            name: 'Leave Credits Batch Upload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Leaves/LeaveCredits/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/leaves/filed-leaves/batch-upload`,
            name: 'Filed Leaves Batch Upload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Leaves/FiledLeaves/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/annual-earnings`,
            name: 'Annual Earnings Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/AnnualEarnings' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/annual-earnings/batch-upload`,
            name: 'Annual Earnings Batch Upload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/AnnualEarnings/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/annual-earnings`,
            name: 'Annual Earnings',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/AnnualEarnings/View/reducer' ),
                    import( 'containers/AnnualEarnings/View/sagas' ),
                    import( 'containers/AnnualEarnings/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'annualEarnings', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/annual-earnings/batch-upload`,
            name: 'Batch Add Annual Earnings',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/AnnualEarnings/BatchUpload/reducer' ),
                    import( 'containers/AnnualEarnings/BatchUpload/sagas' ),
                    import( 'containers/AnnualEarnings/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'batchAddAnnualEarnings', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/annual-earnings/manual-entry`,
            name: 'Add Annual Earnings',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/AnnualEarnings/ManualEntry/reducer' ),
                    import( 'containers/AnnualEarnings/ManualEntry/sagas' ),
                    import( 'containers/AnnualEarnings/ManualEntry' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addAnnualEarnings', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/annual-earnings/:id/detail`,
            name: 'Annual Earnings Details',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/AnnualEarnings/Detail/reducer' ),
                    import( 'containers/AnnualEarnings/Detail/sagas' ),
                    import( 'containers/AnnualEarnings/Detail' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'annualEarningsDetail', viewsReducer.default );
                    injectSagas( viewsSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/annual-earnings/:id/edit`,
            name: 'Edit Annual Earnings',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/AnnualEarnings/Edit/reducer' ),
                    import( 'containers/AnnualEarnings/Edit/sagas' ),
                    import( 'containers/AnnualEarnings/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                // getComponent triggered multiple times
                // https://github.com/ReactTraining/react-router/issues/4266
                if ([ 'POP', 'PUSH' ].indexOf( nextState.location.action ) < 0 ) {
                    return;
                }

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewsReducer, viewsSagas, component ]) => {
                    injectSagas([ ...appSagas.default, ...viewsSagas.default ]);
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectReducer( 'editAnnualEarnings', viewsReducer.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/workflow-entitlements`,
            name: 'Workflow Entitlements Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/WorkflowEntitlements' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/guides/workflow-entitlements/batch-upload`,
            name: 'Workflow Entitlements Batch Upload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/WorkflowEntitlements/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/subscriptions',
            name: 'Subscriptions',
            indexRoute: { onEnter: ( nextState, replace ) => replace( '/control-panel/subscriptions/summary' ) }
        }, {
            path: '/control-panel/subscriptions/summary',
            name: 'Subscription Summary',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Subscriptions/Summary/reducer' ),
                    import( 'containers/Subscriptions/Summary/sagas' ),
                    import( 'containers/Subscriptions/Summary' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'subscriptionSummary', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/subscriptions/licenses',
            name: 'Licenses',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Subscriptions/Licenses/reducer' ),
                    import( 'containers/Subscriptions/Licenses/sagas' ),
                    import( 'containers/Subscriptions/Licenses' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'licenses', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/subscriptions/billing-information',
            name: 'Billing Information',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Subscriptions/BillingInformation/reducer' ),
                    import( 'containers/Subscriptions/BillingInformation/sagas' ),
                    import( 'containers/Subscriptions/BillingInformation' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'billingInformation', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/subscriptions/receipts',
            name: 'Receipts',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Subscriptions/Receipts/reducer' ),
                    import( 'containers/Subscriptions/Receipts/sagas' ),
                    import( 'containers/Subscriptions/Receipts' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'receipts', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/subscriptions/receipts/:id',
            name: 'View Receipt',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Subscriptions/Receipts/View/reducer' ),
                    import( 'containers/Subscriptions/Receipts/View/sagas' ),
                    import( 'containers/Subscriptions/Receipts/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'receipt_view', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/subscriptions/invoices',
            name: 'Invoices',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Subscriptions/Invoices/reducer' ),
                    import( 'containers/Subscriptions/Invoices/sagas' ),
                    import( 'containers/Subscriptions/Invoices' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'invoices', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/subscriptions/invoices/:id',
            name: 'View Invoice',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Subscriptions/Invoices/View/reducer' ),
                    import( 'containers/Subscriptions/Invoices/View/sagas' ),
                    import( 'containers/Subscriptions/Invoices/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'invoice_view', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/subscriptions/account-deletion',
            name: 'Account Deletion',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Subscriptions/AccountDeletion/reducer' ),
                    import( 'containers/Subscriptions/AccountDeletion/sagas' ),
                    import( 'containers/Subscriptions/AccountDeletion' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'accountDeletion', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/company-details',
            name: 'companyDetails',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/CompanyDetails/reducer' ),
                    import( 'containers/CompanyDetails/sagas' ),
                    import( 'containers/CompanyDetails' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'companyDetails', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/time-attendance/day-hour-rates',
            name: 'dayHourRates',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/DayHourRates/view/reducer' ),
                    import( 'containers/DayHourRates/view/sagas' ),
                    import( 'containers/DayHourRates/view' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'dayHourRates', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/time-attendance/maximum-clock-out-rules',
            name: 'maximumClockOutRulesView',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/MaximumClockOutRules/View/reducer' ),
                    import( 'containers/MaximumClockOutRules/View/sagas' ),
                    import( 'containers/MaximumClockOutRules/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'maximumClockOutRulesView', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/time-attendance/maximum-clock-out-rules/add',
            name: 'maximumClockOutRulesAdd',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/MaximumClockOutRules/View/reducer' ),
                    import( 'containers/MaximumClockOutRules/View/sagas' ),
                    import( 'containers/MaximumClockOutRules/Add/reducer' ),
                    import( 'containers/MaximumClockOutRules/Add/sagas' ),
                    import( 'containers/MaximumClockOutRules/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewReducer, viewSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'maximumClockOutRulesView', viewReducer.default );
                    injectSagas( viewSagas.default );
                    injectReducer( 'maximumClockOutRulesAdd', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/ranks',
            name: 'ranks',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Ranks/View/reducer' ),
                    import( 'containers/Ranks/View/sagas' ),
                    import( 'containers/Ranks/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, rankReducer, rankSagas, rankComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'ranks', rankReducer.default );
                    injectSagas( rankSagas.default );
                    renderRoute( rankComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/ranks/add',
            name: 'addRanks',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Ranks/Add/reducer' ),
                    import( 'containers/Ranks/Add/sagas' ),
                    import( 'containers/Ranks/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, addRankReducer, addRankSagas, addRankComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addRanks', addRankReducer.default );
                    injectSagas( addRankSagas.default );
                    renderRoute( addRankComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/employment-types',
            name: 'employmentTypes',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/EmploymentTypes/View/reducer' ),
                    import( 'containers/EmploymentTypes/View/sagas' ),
                    import( 'containers/EmploymentTypes/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'employmentTypes', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/employment-types/add',
            name: 'addEmploymentType',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/EmploymentTypes/Add/reducer' ),
                    import( 'containers/EmploymentTypes/Add/sagas' ),
                    import( 'containers/EmploymentTypes/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, employmentTypeReducer, employmentTypesagas, employmentTypeComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addEmploymentType', employmentTypeReducer.default );
                    injectSagas( employmentTypesagas.default );
                    renderRoute( employmentTypeComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/cost-centers',
            name: 'costCenters',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/CostCenters/View/reducer' ),
                    import( 'containers/CostCenters/View/sagas' ),
                    import( 'containers/CostCenters/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, costCenterReducer, costCenterSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'costCenters', costCenterReducer.default );
                    injectSagas( costCenterSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/cost-centers/add',
            name: 'addCostCenters',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/CostCenters/Add/reducer' ),
                    import( 'containers/CostCenters/Add/sagas' ),
                    import( 'containers/CostCenters/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addCostCenters', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/projects',
            name: 'projects',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Projects/view/reducer' ),
                    import( 'containers/Projects/view/sagas' ),
                    import( 'containers/Projects/view' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'projects', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/projects/add',
            name: 'addProject',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Projects/Add/reducer' ),
                    import( 'containers/Projects/Add/sagas' ),
                    import( 'containers/Projects/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addProject', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/teams',
            name: 'teams',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Teams/View/reducer' ),
                    import( 'containers/Teams/View/sagas' ),
                    import( 'containers/Teams/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, teamReducer, teamSagas, teamComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'teams', teamReducer.default );
                    injectSagas( teamSagas.default );
                    renderRoute( teamComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/teams/add',
            name: 'addTeam',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Teams/Add/reducer' ),
                    import( 'containers/Teams/Add/sagas' ),
                    import( 'containers/Teams/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, teamReducer, teamSagas, teamComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addTeam', teamReducer.default );
                    injectSagas( teamSagas.default );
                    renderRoute( teamComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/teams/:id/edit',
            name: 'editTeams',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Teams/Edit/reducer' ),
                    import( 'containers/Teams/Edit/sagas' ),
                    import( 'containers/Teams/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, teamReducer, teamSagas, teamComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editTeams', teamReducer.default );
                    injectSagas( teamSagas.default );
                    renderRoute( teamComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/payroll-groups',
            name: 'PayrollGroup',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/PayrollGroup/View/reducer' ),
                    import( 'containers/PayrollGroup/View/sagas' ),
                    import( 'containers/PayrollGroup/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'PayrollGroup', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/payroll-groups/add/manual',
            name: 'PayrollGroupAdd',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/PayrollGroup/Add/reducer' ),
                    import( 'containers/PayrollGroup/Add/sagas' ),
                    import( 'containers/PayrollGroup/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'PayrollGroupAdd', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/payroll-groups/:id/edit/manual',
            name: 'PayrollGroupEdit',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/PayrollGroup/Edit/reducer' ),
                    import( 'containers/PayrollGroup/Edit/sagas' ),
                    import( 'containers/PayrollGroup/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'PayrollGroupEdit', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/schedule-settings/tardiness-rules',
            name: 'tardinessRules',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/TardinessRules/View/reducer' ),
                    import( 'containers/TardinessRules/View/sagas' ),
                    import( 'containers/TardinessRules/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'tardinessRules', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/schedule-settings/tardiness-rules/add',
            name: 'addTardinessRules',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/TardinessRules/Add/reducer' ),
                    import( 'containers/TardinessRules/Add/sagas' ),
                    import( 'containers/TardinessRules/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addTardinessRules', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/schedule-settings/tardiness-rules/:id/edit',
            name: 'editTardinessRules',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/TardinessRules/Edit/reducer' ),
                    import( 'containers/TardinessRules/Edit/sagas' ),
                    import( 'containers/TardinessRules/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editTardinessRules', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/allowance-types',
            name: 'AllowanceTypes',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/AllowanceTypes/View/reducer' ),
                    import( 'containers/AllowanceTypes/View/sagas' ),
                    import( 'containers/AllowanceTypes/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'AllowanceTypes', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/commission-types',
            name: 'CommissionType',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/CommissionType/View/reducer' ),
                    import( 'containers/CommissionType/View/sagas' ),
                    import( 'containers/CommissionType/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'CommissionType', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/commission-types/add',
            name: 'CommissionTypeAdd',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/CommissionType/Add/reducer' ),
                    import( 'containers/CommissionType/Add/sagas' ),
                    import( 'containers/CommissionType/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'CommissionTypeAdd', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/allowance-types/add',
            name: 'AllowanceTypeAdd',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/AllowanceTypes/View/reducer' ),
                    import( 'containers/AllowanceTypes/View/sagas' ),
                    import( 'containers/AllowanceTypes/Add/reducer' ),
                    import( 'containers/AllowanceTypes/Add/sagas' ),
                    import( 'containers/AllowanceTypes/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, viewReducer, viewSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'AllowanceTypes', viewReducer.default );
                    injectSagas( viewSagas.default );
                    injectReducer( 'AllowanceTypeAdd', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/leave-settings/leave-types',
            name: 'leaveTypes',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/LeaveTypes/View/reducer' ),
                    import( 'containers/LeaveTypes/View/sagas' ),
                    import( 'containers/LeaveTypes/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'leaveTypes', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/leave-settings/leave-types/add',
            name: 'leaveTypesAdd',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/LeaveTypes/Add/reducer' ),
                    import( 'containers/LeaveTypes/Add/sagas' ),
                    import( 'containers/LeaveTypes/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'leaveTypesAdd', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/leave-settings/leave-types/:id/edit',
            name: 'leaveTypesEdit',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/LeaveTypes/Edit/reducer' ),
                    import( 'containers/LeaveTypes/Edit/sagas' ),
                    import( 'containers/LeaveTypes/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'leaveTypesEdit', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/leave-settings/leave-entitlement',
            name: 'leaveEntitlements',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/LeaveEntitlements/View/reducer' ),
                    import( 'containers/LeaveEntitlements/View/sagas' ),
                    import( 'containers/LeaveEntitlements/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'leaveEntitlements', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/leave-settings/leave-entitlement/:id/edit',
            name: 'editLeaveEntitlements',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/LeaveEntitlements/Edit/reducer' ),
                    import( 'containers/LeaveEntitlements/Edit/sagas' ),
                    import( 'containers/LeaveEntitlements/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editLeaveEntitlements', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/leave-settings/leave-entitlement/add',
            name: 'addLeaveEntitlements',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/LeaveEntitlements/Add/reducer' ),
                    import( 'containers/LeaveEntitlements/Add/sagas' ),
                    import( 'containers/LeaveEntitlements/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addLeaveEntitlements', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/deduction-types',
            name: 'Deduction Types',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/DeductionType/View/reducer' ),
                    import( 'containers/DeductionType/View/sagas' ),
                    import( 'containers/DeductionType/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'deductionType', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/deduction-types/add',
            name: 'add',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/DeductionType/Add/reducer' ),
                    import( 'containers/DeductionType/Add/sagas' ),
                    import( 'containers/DeductionType/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addDeductionType', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/schedule-settings/default-schedule',
            name: 'defaultSchedule',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/DefaultSchedule/reducer' ),
                    import( 'containers/DefaultSchedule/sagas' ),
                    import( 'containers/DefaultSchedule' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'defaultSchedule', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/schedule-settings/night-shift',
            name: 'nightShift',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/NightShift/reducer' ),
                    import( 'containers/NightShift/sagas' ),
                    import( 'containers/NightShift' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'nightShift', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/schedule-settings/holidays',
            name: 'holidays',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Holidays/View/reducer' ),
                    import( 'containers/Holidays/View/sagas' ),
                    import( 'containers/Holidays/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'holidays', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/schedule-settings/holidays/add',
            name: 'addHolidays',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Holidays/Add/reducer' ),
                    import( 'containers/Holidays/Add/sagas' ),
                    import( 'containers/Holidays/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addHolidays', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/schedule-settings/holidays/:id/edit',
            name: 'editHolidays',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Holidays/Edit/reducer' ),
                    import( 'containers/Holidays/Edit/sagas' ),
                    import( 'containers/Holidays/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editHolidays', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/locations',
            name: 'location',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Location/view/reducer' ),
                    import( 'containers/Location/view/sagas' ),
                    import( 'containers/Location/view' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'location', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/locations/add',
            name: 'addLocation',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Location/Add/reducer' ),
                    import( 'containers/Location/Add/sagas' ),
                    import( 'containers/Location/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addLocation', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/locations/:id/edit',
            name: 'editLocation',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Location/Edit/reducer' ),
                    import( 'containers/Location/Edit/sagas' ),
                    import( 'containers/Location/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editLocation', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/bonus-types',
            name: 'Bonus Types',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/BonusType/View/reducer' ),
                    import( 'containers/BonusType/View/sagas' ),
                    import( 'containers/BonusType/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'bonusType', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/bonus-types/add',
            name: 'Bonus Types Add',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/BonusType/Add/reducer' ),
                    import( 'containers/BonusType/Add/sagas' ),
                    import( 'containers/BonusType/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'BonusTypesAdd', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/payroll/bonus-types/:id/edit',
            name: 'Bonus Types Edit',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/BonusType/Edit/reducer' ),
                    import( 'containers/BonusType/Edit/sagas' ),
                    import( 'containers/BonusType/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'BonusTypesEdit', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/workflow-automation/workflows',
            name: 'workflows',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Workflows/View/reducer' ),
                    import( 'containers/Workflows/View/sagas' ),
                    import( 'containers/Workflows/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'workflows', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/workflow-automation/workflows/add',
            name: 'workflowsAdd',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Workflows/Add/reducer' ),
                    import( 'containers/Workflows/Add/sagas' ),
                    import( 'containers/Workflows/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'workflowsAdd', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/workflow-automation/workflows/:id/edit',
            name: 'workflowsEdit',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Workflows/Edit/reducer' ),
                    import( 'containers/Workflows/Edit/sagas' ),
                    import( 'containers/Workflows/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'workflowsEdit', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/organizational-chart',
            name: 'organizationalChart',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/OrganizationalChart/View/reducer' ),
                    import( 'containers/OrganizationalChart/View/sagas' ),
                    import( 'containers/OrganizationalChart/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'organizationalChart', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/organizational-chart/departments/add',
            name: 'organizationalAddDepartments',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/OrganizationalChart/Add/Department/reducer' ),
                    import( 'containers/OrganizationalChart/Add/Department/sagas' ),
                    import( 'containers/OrganizationalChart/Add/Department' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'organizationalAddDepartments', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/organizational-chart/positions/add',
            name: 'addPosition',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/OrganizationalChart/Add/Position/reducer' ),
                    import( 'containers/OrganizationalChart/Add/Position/sagas' ),
                    import( 'containers/OrganizationalChart/Add/Position' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addPosition', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/organizational-chart/departments/:id/edit',
            name: 'organizationalEditDepartments',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/OrganizationalChart/Edit/Department/reducer' ),
                    import( 'containers/OrganizationalChart/Edit/Department/sagas' ),
                    import( 'containers/OrganizationalChart/Edit/Department' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'organizationalEditDepartments', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/company-settings/company-structure/organizational-chart/positions/:id/edit',
            name: 'editPosition',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/OrganizationalChart/Edit/Position/reducer' ),
                    import( 'containers/OrganizationalChart/Edit/Position/sagas' ),
                    import( 'containers/OrganizationalChart/Edit/Position' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editPosition', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/companies',
            name: 'Companies',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Companies/View/reducer' ),
                    import( 'containers/Companies/View/sagas' ),
                    import( 'containers/Companies/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'companies', pageReducer.default );
                    injectSagas( pageSagas.default );

                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/companies/add',
            name: 'Add Companies',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Companies/Add/reducer' ),
                    import( 'containers/Companies/Add/sagas' ),
                    import( 'containers/Companies/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addCompany', pageReducer.default );
                    injectSagas( pageSagas.default );

                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/companies/:id/edit',
            name: 'Edit Companies',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Companies/Edit/reducer' ),
                    import( 'containers/Companies/Edit/sagas' ),
                    import( 'containers/Companies/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editCompany', pageReducer.default );
                    injectSagas( pageSagas.default );

                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/users/management',
            name: 'userManagement',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/UserManagement/reducer' ),
                    import( 'containers/UserManagement/sagas' ),
                    import( 'containers/UserManagement' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'userManagement', pageReducer.default );
                    injectSagas( pageSagas.default );

                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/users/create',
            name: 'userManagement - create',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/UserManagement/reducer' ),
                    import( 'containers/UserManagement/sagas' ),
                    import( 'containers/UserManagement/Add/reducer' ),
                    import( 'containers/UserManagement/Add/sagas' ),
                    import( 'containers/UserManagement/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, listReducer, listSaga, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'userManagement', listReducer.default );
                    injectSagas( listSaga.default );
                    injectReducer( 'createUser', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/users/:userId',
            name: 'userManagement - view',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/UserManagement/View/reducer' ),
                    import( 'containers/UserManagement/View/sagas' ),
                    import( 'containers/UserManagement/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'viewUser', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/users/:userId/edit',
            name: 'userManagement - edit',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/UserManagement/reducer' ),
                    import( 'containers/UserManagement/sagas' ),
                    import( 'containers/UserManagement/Edit/reducer' ),
                    import( 'containers/UserManagement/Edit/sagas' ),
                    import( 'containers/UserManagement/Edit' )

                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, listReducer, listSaga, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'userManagement', listReducer.default );
                    injectSagas( listSaga.default );
                    injectReducer( 'editUser', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/audit-trail',
            name: 'Audit Trail',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/ControlPanelAuditTrail/reducer' ),
                    import( 'containers/ControlPanelAuditTrail/sagas' ),
                    import( 'containers/ControlPanelAuditTrail' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'controlPanelAuditTrail', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/salpay-integration',
            name: 'SALPay Integration',
            indexRoute: { onEnter: ( nextState, replace ) => replace( '/control-panel/salpay-integration/summary' ) }
        }, {
            path: '/control-panel/salpay-integration/summary',
            name: 'SALPay Integration - Summary',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/SalPayIntegration/Summary/reducer' ),
                    import( 'containers/SalPayIntegration/Summary/sagas' ),
                    import( 'containers/SalPayIntegration/Summary' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'salpayIntegration', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/salpay-integration/payees',
            name: 'SALPay Integration - Payees',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/SalPayIntegration/Payees/reducer' ),
                    import( 'containers/SalPayIntegration/Payees/sagas' ),
                    import( 'containers/SalPayIntegration/Payees' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'salpayPayees', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/salpay-integration/administrators',
            name: 'SALPay Integration - Administrators',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/SalPayIntegration/Administrators/reducer' ),
                    import( 'containers/SalPayIntegration/Administrators/sagas' ),
                    import( 'containers/SalPayIntegration/Administrators' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'salpayAdministrators', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/salpay-integration/companies/:id/details',
            name: 'SALPay Integration - Company Details',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/SalPayIntegration/Details/reducer' ),
                    import( 'containers/SalPayIntegration/Details/sagas' ),
                    import( 'containers/SalPayIntegration/Details' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'salpayCompanyDetails', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/salpay-integration/companies/:id/link',
            name: 'SALPay Integration - Link Company',
            indexRoute: { onEnter: ( nextState, replace ) =>
                !( nextState.location.state && Object.prototype.hasOwnProperty.call( nextState.location.state, 'account_details' ) )
                    && replace( '/control-panel/salpay-integration/companies' )
            },
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/SalPayIntegration/Link/reducer' ),
                    import( 'containers/SalPayIntegration/Link/sagas' ),
                    import( 'containers/SalPayIntegration/Link' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, pageReducer, pageSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'salpayCompanyLink', pageReducer.default );
                    injectSagas( pageSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/device-management',
            name: 'deviceManagement',
            indexRoute: { onEnter: ( nextState, replace ) => replace( '/control-panel/device-management/users' ) }
        }, {
            path: '/control-panel/device-management/users',
            name: 'deviceManagement - users',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/DeviceManagement/ManageUser/View/reducer' ),
                    import( 'containers/DeviceManagement/ManageUser/View/sagas' ),
                    import( 'containers/DeviceManagement/ManageUser/View' )
                ]);
                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, deviceReducer, deviceSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'deviceManagement', deviceReducer.default );
                    injectSagas( deviceSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/device-management/users/:userId',
            name: 'deviceManagement - users',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/DeviceManagement/ManageUser/ViewUserDetail/reducer' ),
                    import( 'containers/DeviceManagement/ManageUser/ViewUserDetail/sagas' ),
                    import( 'containers/DeviceManagement/ManageUser/ViewUserDetail' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, viewReducer, viewSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'viewUser', viewReducer.default );
                    injectSagas( viewSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/device-management/unlinked-users',
            name: 'deviceManagement - unlinked-users',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/DeviceManagement/ManageUnlinkUser/reducer' ),
                    import( 'containers/DeviceManagement/ManageUnlinkUser/sagas' ),
                    import( 'containers/DeviceManagement/ManageUnlinkUser' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, unlinkUserReducer, unlinkUserSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'unlinkUser', unlinkUserReducer.default );
                    injectSagas( unlinkUserSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/roles',
            name: 'userRoles',
            indexRoute: { onEnter: ( nextState, replace ) => replace( '/control-panel/roles/user-roles' ) }
        }, {
            path: '/control-panel/roles/user-roles',
            name: 'UserRoles',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/UserRoles/reducer' ),
                    import( 'containers/UserRoles/sagas' ),
                    import( 'containers/UserRoles' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'UserRoles', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/roles/role-create',
            name: 'createRoles',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/UserRoles/Create/reducer' ),
                    import( 'containers/UserRoles/Create/sagas' ),
                    import( 'containers/UserRoles/Create' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'createRoles', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/control-panel/roles/:roleId/role-edit',
            name: 'editRoles',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/UserRoles/Edit/reducer' ),
                    import( 'containers/UserRoles/Edit/sagas' ),
                    import( 'containers/UserRoles/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editRoles', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME_MF}/download(/:module/:id)`,
            name: 'Download View',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Download/reducer' ),
                    import( 'containers/Download/sagas' ),
                    import( 'containers/Download' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, downloadReducer, downloadSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'download', downloadReducer.default );
                    injectSagas( downloadSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: `${BASE_PATH_NAME}/unauthorized`,
            name: 'Unauthorized',
            // anyDevice: true,
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Unauthorized' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '/time/shifts/calendar',
            name: 'shift',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Shift/View/reducer' ),
                    import( 'containers/Shift/View/sagas' ),
                    import( 'containers/Shift/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, shiftReducer, shiftSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'shift', shiftReducer.default );
                    injectSagas( shiftSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/shifts/assign/batch',
            name: 'assignShifts',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Shift/BatchUpload/reducer' ),
                    import( 'containers/Shift/BatchUpload/sagas' ),
                    import( 'containers/Shift/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, shiftReducer, shiftSagas, shiftComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'assignShifts', shiftReducer.default );
                    injectSagas( shiftSagas.default );
                    renderRoute( shiftComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/shifts/assign/batch/guide',
            name: 'shiftTemplete',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Shift/Template/reducer' ),
                    import( 'containers/Shift/Template/sagas' ),
                    import( 'containers/Shift/Template' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'shiftTemplete', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/shifts/assign/manual',
            name: 'addShifts',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Shift/Add/reducer' ),
                    import( 'containers/Shift/Add/sagas' ),
                    import( 'containers/Shift/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, shiftReducer, shiftSagas, shiftComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addShifts', shiftReducer.default );
                    injectSagas( shiftSagas.default );
                    renderRoute( shiftComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/shifts/edit-rest-day/:id',
            name: 'editRestday',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Shift/EditRestday/reducer' ),
                    import( 'containers/Shift/EditRestday/sagas' ),
                    import( 'containers/Shift/EditRestday' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, shiftReducer, shiftSagas, shiftComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editRestday', shiftReducer.default );
                    injectSagas( shiftSagas.default );
                    renderRoute( shiftComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/shifts/edit-shift/:id',
            name: 'editShift',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Shift/EditShift/reducer' ),
                    import( 'containers/Shift/EditShift/sagas' ),
                    import( 'containers/Shift/EditShift' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, editShiftReducer, editShiftSagas, editShiftComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editShift', editShiftReducer.default );
                    injectSagas( editShiftSagas.default );
                    renderRoute( editShiftComponent );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '/time/schedules',
            name: 'schedules',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Schedules/View/reducer' ),
                    import( 'containers/Schedules/View/sagas' ),
                    import( 'containers/Schedules/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, schedulesReducer, schedulesSagas, schedulesComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'schedules', schedulesReducer.default );
                    injectSagas( schedulesSagas.default );
                    renderRoute( schedulesComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/schedules/add',
            name: 'addSchedule',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Schedules/Add/reducer' ),
                    import( 'containers/Schedules/Add/sagas' ),
                    import( 'containers/Schedules/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, addSchedulesReducer, addSchedulesSagas, addSchedulesComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'addSchedule', addSchedulesReducer.default );
                    injectSagas( addSchedulesSagas.default );
                    renderRoute( addSchedulesComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/schedules/batch-update',
            name: 'batchEditSchedule',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Schedules/BatchUpdate/reducer' ),
                    import( 'containers/Schedules/BatchUpdate/sagas' ),
                    import( 'containers/Schedules/BatchUpdate' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, batchEditSchedulesReducer, batchEditSchedulesSagas, batchEditSchedulesComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'batchEditSchedule', batchEditSchedulesReducer.default );
                    injectSagas( batchEditSchedulesSagas.default );
                    renderRoute( batchEditSchedulesComponent );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/schedules/edit/:id',
            name: 'editSchedule',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Schedules/Edit/reducer' ),
                    import( 'containers/Schedules/Edit/sagas' ),
                    import( 'containers/Schedules/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, editSchedulesReducer, editSchedulesSagas, editSchedulesComponent ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'editSchedule', editSchedulesReducer.default );
                    injectSagas( editSchedulesSagas.default );
                    renderRoute( editSchedulesComponent );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '/guides/schedules/batch-upload',
            name: 'Schedules Batch Upload Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Schedules/BatchUpload' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/guides/schedules/batch-update',
            name: 'Schedules Batch Update Guide',
            getComponent( location, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Guide/Schedules/BatchUpdate' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '/time/company/announcements/received',
            name: 'announcements',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Announcements/View/reducer' ),
                    import( 'containers/Announcements/View/sagas' ),
                    import( 'containers/Announcements/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'announcements', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }, {
            path: '/time/company/announcements/sent',
            name: 'announcementsSent',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Announcements/Sent/reducer' ),
                    import( 'containers/Announcements/Sent/sagas' ),
                    import( 'containers/Announcements/Sent' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'announcementsSent', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '/company-settings/company-structure/departments',
            name: 'departments',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Departments/View/reducer' ),
                    import( 'containers/Departments/View/sagas' ),
                    import( 'containers/Departments/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'departments', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '/company-settings/company-structure/departments/add',
            name: 'departmentsAdd',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Departments/Add/reducer' ),
                    import( 'containers/Departments/Add/sagas' ),
                    import( 'containers/Departments/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'departmentsAdd', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '/company-settings/company-structure/departments/:departmentId/edit',
            name: 'departmentsEdit',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Departments/Edit/reducer' ),
                    import( 'containers/Departments/Edit/sagas' ),
                    import( 'containers/Departments/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'departmentsEdit', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '/company-settings/company-structure/positions',
            name: 'positions',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Positions/View/reducer' ),
                    import( 'containers/Positions/View/sagas' ),
                    import( 'containers/Positions/View' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'positions', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '/company-settings/company-structure/positions/add',
            name: 'positionsAdd',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Positions/Add/reducer' ),
                    import( 'containers/Positions/Add/sagas' ),
                    import( 'containers/Positions/Add' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'positionsAdd', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '/company-settings/company-structure/positions/:positionId/edit',
            name: 'positionsEdit',
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/Positions/Edit/reducer' ),
                    import( 'containers/Positions/Edit/sagas' ),
                    import( 'containers/Positions/Edit' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, appSagas, reducer, sagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectSagas( appSagas.default );
                    injectReducer( 'positionsEdit', reducer.default );
                    injectSagas( sagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        },
        {
            path: '*',
            name: 'Not Found',
            // anyDevice: true,
            getComponent( nextState, cb ) {
                const importModules = Promise.all([
                    import( 'containers/App/reducer' ),
                    import( 'components/IdleTimer/reducer' ),
                    import( 'components/DemoCompany/reducer' ),
                    import( 'components/DemoCompany/sagas' ),
                    import( 'containers/App/sagas' ),
                    import( 'containers/NotFoundPage' )
                ]);

                const renderRoute = loadModule( cb );

                importModules.then( ([ appReducer, idleTimerReducer, demoCompanyReducer, demoCompanySagas, appSagas, component ]) => {
                    injectReducer( 'topLevel', appReducer.default );
                    injectReducer( 'idleTimer', idleTimerReducer.default );
                    injectReducer( 'demoCompany', demoCompanyReducer.default );
                    injectSagas( demoCompanySagas.default );
                    injectSagas( appSagas.default );
                    renderRoute( component );
                });

                importModules.catch( errorLoading );
            }
        }
    ];
}
