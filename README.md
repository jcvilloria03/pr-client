<div align="center">
  <!-- Build Status -->
  <a href="https://circleci.com/gh/SalariumPH/salarium/tree/master"><img src="https://circleci.com/gh/SalariumPH/salarium/tree/master.svg?style=shield&circle-token=183af6cac2102ee20a0e492afbb43d854f9a202a" /></a>
  <!-- Test Coverage -->
  [COVERAGE BADGE]
  <!-- Version -->
  [RELEASE BADGE]
</div>

<br />
 

<div align="center">
  <sub>Created by <a href="https://www.salarium.com/">Salarium</a> and maintained with ❤️ by an amazing <a href="https://github.com/orgs/SalariumPH/teams/engineering">team of developers</a>.</sub>
</div>
 
## Quick start

1. Clone this repo using `git clone git@github.com:SalariumPH/salarium.git`
1. Run `npm run install` to install dependencies.<br />
  *At this point you can run `npm start` to see the example app at `http://localhost:3000`.*

  *make sure you run the install with sudo (or run the terminal as administrator if you're using windows) to have pre-commit hooks created upon installation.*

Now you're ready to rumble!

> Please note that this boilerplate is **production-ready and not meant for beginners**! If you're just starting out with react or redux, please refer to https://github.com/petehunt/react-howto instead. If you want a solid, battle-tested base to build your next product upon and have some experience with react, this is the perfect start for you.

## Documentation

- [**The Hitchhikers Guide to `react-boilerplate`**](docs/general/introduction.md): An introduction for newcomers to this boilerplate.
- [Overview](docs/general): A short overview of the included tools
- [**Commands**](docs/general/commands.md): Getting the most out of this boilerplate
- [Testing](docs/testing): How to work with the built-in test harness
- [Styling](docs/css): How to work with the CSS tooling
- [Your app](docs/js): Supercharging your app with Routing, Redux, simple
  asynchronicity helpers, etc.

## License

This project is licensed under the UNLICENSED license, Copyright (c) 2017 Salarium. For more information see `LICENSE.md`.

 
