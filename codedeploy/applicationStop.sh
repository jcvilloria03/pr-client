#!/bin/bash

# Disable Nginx in preparation of deploy
service nginx stop
 
# Backup old version of Application
mv /srv/pr-client /srv/pr-client_0
mkdir /srv/pr-client
chown -R slmuser:slmuser /srv/