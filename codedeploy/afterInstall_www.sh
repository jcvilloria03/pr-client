#!/bin/bash

# Build Application
cd /srv/pr-client

# For commits/merges to the DEV branch pull the ENV file from its S3 Bucket
if [ "$DEPLOYMENT_GROUP_NAME" == "frontend-dev" ]
then
    aws s3 cp s3://codedeploy-us-west-2-frontendv3-env-dev/dev.env .env
fi

# For commits/merges to the STG branch pull the ENV file from its S3 Bucket
if [ "$DEPLOYMENT_GROUP_NAME" == "pr-client-stg" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-stg-pr-client.env .env
fi

# For commits/merges to the FT branch pull the ENV file from its S3 Bucket
if [ "$DEPLOYMENT_GROUP_NAME" == "pr-client-ft" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-ft-pr-client.env .env
fi

# For commits/merges to the DEMO branch pull the ENV file from its S3 Bucket
if [ "$DEPLOYMENT_GROUP_NAME" == "pr-client-demo" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-demo-pr-client.env .env
fi

npm install
npm run build
