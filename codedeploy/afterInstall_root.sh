#!/bin/bash

# Cleanup Some non-essentials files
cd /srv
chown slmuser:slmuser -vR pr-client
cd pr-client
rm -rvf -R node_modules/

# Remove previous build
rm -rf /srv/pr-client_0